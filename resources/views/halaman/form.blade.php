@extends('layout.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
    <h1>Halaman Form</h1>
    <form action="/welcome" method="post">
        @csrf
        <label>Nama Lengkap</label><br><br>
        <input type="text" name="nama"><br><br>
        <label>Alamat</label><br><br>
        <textarea name="address" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="kirim">
    </form>
@endsection